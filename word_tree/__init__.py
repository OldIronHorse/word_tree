from .word_tree import make_word_tree, next_char

__version__ = '2.0.0'
